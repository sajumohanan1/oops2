﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Demo
{
    internal class ScientistFrog : Frog, IClimb //ScientistFrog class inheriting from frog class
    {
        public string Subject { get; set; }
        //a specific behaviour of ScientistFrog
        public void Experiment()
        {
            Console.WriteLine($"Scientist frogs do {Subject} experiments");
        }



        //Overloaded constructor
        //add name and weight are the parameters inherited from parent class(Frog)
        public ScientistFrog(string name, double weight, string subject): base(name, weight)
        {
            Subject = subject;
        }
        public override void Eat()
        {
            Console.WriteLine("Scientist frog eat some worms");
        }
        public void Climb()
        {
            Console.WriteLine("Climbing....");
        }

    }
}
