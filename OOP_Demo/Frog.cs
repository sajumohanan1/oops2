﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Demo
{
    abstract class Frog //Frog class is used to define object
    {
        public abstract void Eat(); //just create an abstract method - method signature
       
        /* format of work
         * (1) Default constructor (implicit) + Overloaded constructor
         * (2) Variable (fields)
         * (3) Methods (Behaviors)
         * (4) Properties
         */

        /* Access modifier
         * (1) Public
         * (2) Private
         * (3) Protected 
         */

       // public string name; //varible is created for name

       public string Name { get; set; } //set property -> sytatic sugar

        //public string Name
        //{
        //    get
        //    {
        //        return name;//name variable can only be provided by the Name property which returns name
        //    }
        //    set
        //    {
        //        name = value;//Any attempts to change the value can only be dome by the Name property which sets the name value
        //    }
        //}



        //public double weight;
        public double Weight { get; set; }

        // FrogObject1 - member of frog class
        
        public Frog() //default constructor - explicit
        {
            //execute codes whenever an object is created 
        }


        //Overloaded construct has been created
        public Frog(string name, double weight)
        {
            Name = name;
            Weight = weight;
        }

        //add a behavior for the frog - frog can croak
        public void Croak() //A method is created for the croak behavior of the frog
            //void - not return anything
            //if you keep reutrn type - you should use 'return' keyword.
        {
            //here is the actual behavior of the frog
            Console.WriteLine($"{this.Name} can Croak");
        }

        public virtual void Hop(double distanceCm)//using overridden method->polymorphism
        //use virtual keyword in main class (base class)
        {
            Console.WriteLine($"{this.Name} hopped {distanceCm} cm away");
        }

    }
}

