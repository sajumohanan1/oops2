﻿using System;

namespace OOP_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Object Oriented Programming
            /* Abstraction - default
             * Encapsulation - default
             * Inheritance (Optional) - for the code reusability, easy for code maintenance
             * Polymorphism      (Optional) - Poly -> many and Morph -> Forms  
             *      two approaches to perform Polymorphism 
             *      (1) by using Overriden method (virtual-overriden)
             *      (2) by using Abstract method (abstract) - parent class forced the creation of the child class's behaviour
             Interfaces - an alternative for inheritance, because c# cannot support multiple inheritance
             */

            //Frog - behavior (name, weight, hop, croak)

            //In this main program, you can create objects
            //syntax to create an objec t
            //Frog FrogObject1 = new Frog("Tree Frog",156.47);//this is an object for the frog class
            //Console.WriteLine(FrogObject1.Name);
            //Console.WriteLine(FrogObject1.Weight);
            //FrogObject1.Croak();
            //FrogObject1.Hop(100);


            ////Create another object
            //Frog FrogObject2 = new Frog();
            //FrogObject2.Name = "Glass Frog";
            //FrogObject2.Weight = 190.66;
            //Console.WriteLine(FrogObject2.Name);
            //Console.WriteLine(FrogObject2.Weight);
            //FrogObject2.Croak();
            //FrogObject2.Hop(120); //pass argument (input) for distance in cm

            ScientistFrog scientistFrogObj = new ScientistFrog("Tree Frog", 156.47, "Science");           
            scientistFrogObj.Hop(100);//from main class
           // scientistFrogObj.Experiment(); //from child class
            //Console.WriteLine(scientistFrogObj.Name + ", " + scientistFrogObj.Weight);

            MilitaryFrog militaryFrogObj = new MilitaryFrog();
            militaryFrogObj.Name = "Glas Frog";
            militaryFrogObj.Hop(120); //120 changed as 180 -example for polymorphism
                                      //militaryFrogObj.Salute();
                                      // militaryFrogObj.DigTench();

            //cannot create object for frog class if it is changed as abstract 
            //cannot create object for the abstract class
            //Frog frogObject = new Frog("Tree Frog", 90);
            //frogObject.Croak();
            scientistFrogObj.Eat();
       
        }
    }
}
