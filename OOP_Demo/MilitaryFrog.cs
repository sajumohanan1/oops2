﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Demo
{
    internal class MilitaryFrog : Frog, IClimb //Inheritance
    {
        public string Rank { get; set; }
        //specific behaviors like Salute, Dig Trench

        public void Salute()
        {
            Console.WriteLine($"{Name} reporting for duty");
        }

        public void DigTench()
        {
            Console.WriteLine("Frogs can dig trench");
        }

        public override void Hop(double distanceCm)//using overridden method->polymorphism
        //use override keyword in the child class
        {
            Console.WriteLine($"{this.Name} hopped {distanceCm * 1.5} cm away");
        }
        public override void Eat()
        {
            Console.WriteLine("Scientist frog eat some more worms");
        }

        public void Climb()
        {
            Console.WriteLine("Climbing....");
        }
    }
}
